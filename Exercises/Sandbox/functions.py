import numpy as np
import winsound
import cv2
import os
from scipy import signal


def GaussianFunction(cord, a):
    x, y = cord
    sqrSum = x**2 + y**2
    return -2*np.cos(a)*x*np.exp(-sqrSum)-2*np.sin(a)*y*np.exp(-sqrSum)


def SteerableFilter(img, kernel_size, angle):
    filter = [[GaussianFunction((i - int(kernel_size/2), j - int(kernel_size/2)), angle) for i in range(kernel_size)] for j in range(kernel_size)]
    final = signal.convolve2d(img, filter, mode='same')
    return final


