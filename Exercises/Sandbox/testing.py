import numpy as np
from scipy import signal
import cv2
import glob
import functions as f


circle = cv2.imread('./Exercises/Sandbox/Pictures/Circle.png', cv2.IMREAD_GRAYSCALE)
square = cv2.imread('./Exercises/Sandbox/Pictures/Square.png', cv2.IMREAD_GRAYSCALE)

 
circleA = f.SteerableFilter(circle, 5, 0.25*np.pi)
squareA = f.SteerableFilter(square, 5, 0.25*np.pi)

cv2.imshow("circle", circle)
cv2.waitKey(0)
cv2.imshow("circleA", circleA)
cv2.waitKey(0)
cv2.imshow("square", square)
cv2.waitKey(0)
cv2.imshow("squareA", squareA)
cv2.waitKey(0)