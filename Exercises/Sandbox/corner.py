import numpy as np
from scipy import signal
import cv2
import glob


circle = cv2.imread('./Exercises/Sandbox/Pictures/Circle.png', cv2.IMREAD_GRAYSCALE)
square = cv2.imread('./Exercises/Sandbox/Pictures/Square.png', cv2.IMREAD_GRAYSCALE)


a = np.array([
    [1,-2,1],
    [-2,4,-2],
    [1,-2,1]
])*0.25

circleA = signal.convolve2d(circle, a, mode='same')
squareA = signal.convolve2d(square, a, mode='same')

cv2.imshow("circleA", circleA)
cv2.waitKey(0)
cv2.imshow("squareA", squareA)
cv2.waitKey(0)