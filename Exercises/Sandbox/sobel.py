import numpy as np
import cv2
import glob
from scipy import signal


circle = cv2.imread('./Exercises/Sandbox/Pictures/Circle.png', cv2.IMREAD_GRAYSCALE)
square = cv2.imread('./Exercises/Sandbox/Pictures/Square.png', cv2.IMREAD_GRAYSCALE)


a = np.array([
    [1,0,-1],
    [2,0,-2],
    [1,0,-1]
])*0.125
b = np.array([
    [-1,0,1],
    [-2,0,2],
    [-1,0,1]
])*0.125

circleA = signal.convolve2d(circle, a, mode='same')
circleB = signal.convolve2d(circle, b, mode='same')
squareA = signal.convolve2d(square, a, mode='same')
squareB = signal.convolve2d(square, b, mode='same')

cv2.imshow("circleA", circleA)
cv2.waitKey(0)
cv2.imshow("circleB", circleB)
cv2.waitKey(0)
cv2.imshow("squareA", squareA)
cv2.waitKey(0)
cv2.imshow("squareB", squareB)
cv2.waitKey(0)
