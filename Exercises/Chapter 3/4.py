import cv2
import numpy as np


grScrBgr = cv2.imread('./Exercises/Chapter 3/Pictures/Background.png')
grScr = cv2.imread('./Exercises/Chapter 3/Pictures/Test.png')
background = cv2.imread('./Exercises/Chapter 3/Pictures/Stream.jpg')
background = cv2.resize(background, (grScrBgr.shape[1], grScrBgr.shape[0]))

########  get mask ########

grScrBgr = grScrBgr.astype('int16')
grScr = grScr.astype('int16')

mask = np.abs(grScr - grScrBgr).astype('uint8')
maskB = cv2.threshold(mask[:,:,0], 50, 1, cv2.THRESH_BINARY)[1]
maskG = cv2.threshold(mask[:,:,1], 50, 1, cv2.THRESH_BINARY)[1]
maskR = cv2.threshold(mask[:,:,2], 50, 1, cv2.THRESH_BINARY)[1]
mask = np.clip(maskB+maskG+maskR, 0, 1)
mask = np.dstack((mask, mask, mask))
invMask = np.ones(mask.shape) - mask
invMask = invMask.astype('uint8')


final = (grScr * mask + background * invMask).astype('uint8')
cv2.imwrite('./Exercises/Chapter 3/Pictures/final.png', final)

cv2.imshow('mask', final)
cv2.waitKey(0)

