import cv2
import numpy as np
import matplotlib.pyplot as plt


img = cv2.imread('./Pictures/Stream.jpg')
img = cv2.resize(img, (1280, 720))

########  7.1 #######

imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

########  7.2 #######

data = imgGray.ravel()

count, bins_count = np.histogram(data, bins=256) 
cumcount = np.cumsum(count)

plt.hist(data, 256, [0, 256], color='gray')
plt.show()

plt.plot(bins_count[1:], count, color='blue')
plt.plot(bins_count[1:], cumcount, color='red')
plt.show()

pdf = count/sum(count)
cdf = np.cumsum(pdf)

plt.plot(bins_count[1:], pdf, color='blue')
plt.plot(bins_count[1:], cdf, color='red')
plt.show()

