import numpy as np
import cv2
import functions as f


circle = cv2.imread('./Exercises/Chapter 3/Pictures/Circle.png', cv2.IMREAD_GRAYSCALE)
square = cv2.imread('./Exercises/Chapter 3/Pictures/Square.png', cv2.IMREAD_GRAYSCALE)
streamGray = cv2.imread('./Exercises/Chapter 3/Pictures/stream.jpg', cv2.IMREAD_GRAYSCALE)
stream = cv2.imread('./Exercises/Chapter 3/Pictures/stream.jpg')

 
circleA = f.SteerableFilter(circle, 5, 0.25*np.pi)
squareA = f.SteerableFilter(square, 5, 0.25*np.pi)
streamGrayA = f.SteerableFilter(streamGray, 5, 0.25*np.pi)
streamA = f.SteerableFilter(stream, 5, 0.25*np.pi)

cv2.imshow("circle", circle)
cv2.waitKey(0)
cv2.imshow("circleA", circleA)
cv2.waitKey(0)  
cv2.imshow("square", square)
cv2.waitKey(0)
cv2.imshow("squareA", squareA)
cv2.waitKey(0)
cv2.imshow("StreamGray", streamGray)
cv2.waitKey(0)
cv2.imshow("StreamGrayA", streamGrayA)
cv2.waitKey(0)
cv2.imshow("Stream", stream)
cv2.waitKey(0)
cv2.imshow("StreamA", streamA)
cv2.waitKey(0)