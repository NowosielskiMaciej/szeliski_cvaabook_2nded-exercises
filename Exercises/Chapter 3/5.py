import cv2
import numpy as np


capBck = cv2.VideoCapture('./Pictures/background.mp4')
capObj = cv2.VideoCapture('./Pictures/Object.mp4')
background = cv2.imread('./Pictures/Stream.jpg')
background = cv2.resize(background, (640,480))

ret, frame = capBck.read()
frame = cv2.resize(frame, (640,480))
blue = frame[:,:,0]
green = frame[:,:,1]
red = frame[:,:,2]

n = 0

while capBck.isOpened():
    ret, frame = capBck.read()
    frame = cv2.resize(frame, (640,480))
    blue = np.dstack((blue, frame[:,:,0]))
    green = np.dstack((green, frame[:,:,1]))
    red = np.dstack((red, frame[:,:,2]))
    
    n += 1
    print(n)
    if n == 10:
        break
capBck.release()

meanB = np.mean(blue, axis=2)
meanG = np.mean(green, axis=2)
meanR = np.mean(red, axis=2)
mean = np.dstack((meanB,meanG,meanR))

varB = np.var(blue, axis=2)
varG = np.var(green, axis=2)
varR = np.var(red, axis=2)
var = np.dstack((varB,varG,varR))

mean = mean.astype('float')
var = var.astype('float')

del meanB,meanG,meanR,varB,varG,varR,blue,green,red

n = 0

while capObj.isOpened():
    ret, frame = capObj.read()
    frame = frame.astype('float')
    frame = cv2.resize(frame, (640,480))
    mask = np.abs(np.abs(frame - mean) - var).astype('uint8')
    maskB = cv2.threshold(mask[:,:,0], 10, 1, cv2.THRESH_BINARY)[1]
    maskG = cv2.threshold(mask[:,:,1], 10, 1, cv2.THRESH_BINARY)[1]
    maskR = cv2.threshold(mask[:,:,2], 10, 1, cv2.THRESH_BINARY)[1]
    mask = np.clip(maskB+maskG+maskR, 0, 1)
    mask = np.dstack((mask, mask, mask))
    invMask = np.ones(mask.shape) - mask
    invMask = invMask.astype('uint8')

    final = (frame * mask + background * invMask).astype('uint8')
    final = cv2.resize(final, (640,480))
    n += 1
    print(n)
    cv2.imshow('Final', final)
    if cv2.waitKey(1) & 0xFF == ord('q'): 
        break

capObj.release()
cv2.destroyAllWindows()
