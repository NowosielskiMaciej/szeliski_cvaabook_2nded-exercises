import cv2
import matplotlib as plt
import numpy as np


original = cv2.imread('./Pictures/Stream.jpg')
original = cv2.resize(original, (1280, 720))
original = original.astype('uint16')

brightness = np.clip(original + 30, 0, 255)

contrast = np.clip(original * 1.2, 0, 255)

gamma = np.clip(original ** (1/1.2), 0, 255)

blue = cv2.equalizeHist(original[:,:,0].astype('uint8'))
green = cv2.equalizeHist(original[:,:,1].astype('uint8'))
red = cv2.equalizeHist(original[:,:,2].astype('uint8'))
equlize = np.dstack((blue, green, red))

clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(20,20))
blue = clahe.apply(original[:,:,0].astype('uint8'))
green = clahe.apply(original[:,:,1].astype('uint8'))
red = clahe.apply(original[:,:,2].astype('uint8'))
claheImg = np.dstack((blue, green, red))

original = original.astype('uint8')
cv2.imshow('orignal', original)
cv2.waitKey(0)

brightness = brightness.astype('uint8')
cv2.imshow('brightness', brightness)
cv2.waitKey(0)

contrast = contrast.astype('uint8')
cv2.imshow('contrast', contrast)
cv2.waitKey(0)

gamma = gamma.astype('uint8')
cv2.imshow('gamma', gamma)
cv2.waitKey(0)

equlize = equlize.astype('uint8')
cv2.imshow('equlize', equlize)
cv2.waitKey(0)

claheImg = claheImg.astype('uint8')
cv2.imshow('claheImg', claheImg)
cv2.waitKey(0)
 