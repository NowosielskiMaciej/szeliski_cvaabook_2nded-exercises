from PIL import ImageGrab
import numpy as np
import cv2


# Capture the entire screen
screenshot = ImageGrab.grab()

# # Save the screenshot to a file
# screenshot.save("screenshot.png")

arr = np.array(screenshot)

arr = cv2.resize(arr, (640, 480))

cv2.imwrite("C:/Users/Maciek/Projekty/Test/Pictures/Test.png", arr)

# cv2.imshow("arr", arr)
# cv2.waitKey(0)

# Close the screenshot
screenshot.close()
