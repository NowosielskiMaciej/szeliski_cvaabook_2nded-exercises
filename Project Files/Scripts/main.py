from PIL import ImageGrab
import numpy as np
import cv2
import functions as f


img = cv2.imread("C:/Users/Maciek/Projekty/Test/Pictures/stream.jpg")

img = cv2.resize(img, (1280, 720))

imgCopy = img.copy()

imgGrey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

imgGrey2 = imgGrey // 64 * 64
imgCanny = cv2.Canny(imgGrey2, 80, 160)
imgDilate = cv2.dilate(imgCanny, (7, 7))
imgWrapped, n = f.getChessBoard(imgCopy, imgDilate)

# cv2.imshow("img", img)
# cv2.waitKey(0)

# cv2.imshow("img", imgGrey)
# cv2.waitKey(0) 

# cv2.imshow("img", imgGrey2)
# cv2.waitKey(0) 

# cv2.imshow("img", imgCanny)
# cv2.waitKey(0)

cv2.imshow("img", imgDilate)
cv2.waitKey(0)

cv2.imshow("img", imgWrapped)
cv2.waitKey(0)

print(n)
 